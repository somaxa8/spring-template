package com.somaxa8.shared.web.constraints

import jakarta.validation.Constraint
import jakarta.validation.Payload
import kotlin.reflect.KClass

@Target(AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
@Constraint(validatedBy = [ValidFileExtensionsValidator::class])
annotation class ValidFileExtensions(
    val message: String = "Extension is not valid",
    val extensions: Array<String>,
    val groups: Array<KClass<*>> = [],
    val payload: Array<KClass<out Payload>> = []
)
