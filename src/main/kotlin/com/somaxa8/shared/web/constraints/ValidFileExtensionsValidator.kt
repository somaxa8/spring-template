package com.somaxa8.shared.web.constraints

import jakarta.validation.ConstraintValidator
import jakarta.validation.ConstraintValidatorContext
import org.springframework.web.multipart.MultipartFile


class ValidFileExtensionsValidator : ConstraintValidator<ValidFileExtensions, MultipartFile> {

    private lateinit var allowedExtensions: Array<String>

    override fun initialize(constraint: ValidFileExtensions) {
        allowedExtensions = constraint.extensions
    }

    override fun isValid(file: MultipartFile?, context: ConstraintValidatorContext): Boolean {
        return file?.originalFilename?.let { filename ->
            !file.isEmpty && allowedExtensions.any { filename.endsWith(it) }
        } ?: true
    }
}
