package com.somaxa8.shared.web.utils

import org.springframework.data.domain.PageRequest
import org.springframework.web.context.request.NativeWebRequest

object PageRequestBuilder {

    const val API_REQUEST_DEFAULT_PAGE_SIZE = 20
    const val API_REQUEST_DEFAULT_PAGE = 0

    public fun build(request: NativeWebRequest): PageRequest {

        val page = request.getParameter("page")?.toIntOrNull() ?: API_REQUEST_DEFAULT_PAGE
        val pageSize = request.getParameter("pageSize")?.toIntOrNull() ?: API_REQUEST_DEFAULT_PAGE_SIZE

        return PageRequest.of(page, pageSize)
    }
}