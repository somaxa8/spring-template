package com.somaxa8.shared.exception.exceptions

open class ApplicationException(message: String?) : RuntimeException(message)