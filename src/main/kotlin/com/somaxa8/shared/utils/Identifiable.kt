package com.somaxa8.shared.utils

interface Identifiable<T> {
    var id: T?
}