package com.somaxa8.template.service

import com.somaxa8.template.domain.models.JwtToken
import com.somaxa8.template.domain.models.UserCredentials
import com.somaxa8.template.domain.models.UserIdentity
import com.somaxa8.template.security.util.JwtTokenUtil
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service

@Service
class AuthService(
    private val _userService: UserService,
    private val _authenticationManager: AuthenticationManager,
    private val _passwordEncoder: PasswordEncoder,
    private val _jwtTokenUtil: JwtTokenUtil,
) {

    fun authenticate(credentials: UserCredentials): JwtToken {
        val user = _userService.findByEmail(credentials.email)

        if (!_passwordEncoder.matches(credentials.password, user.password)) {
            throw BadCredentialsException("Bad credentials")
        }

        val userIdentity = UserIdentity(email = user.email!!, user.id!!)

        _authenticationManager.authenticate(
            UsernamePasswordAuthenticationToken(userIdentity, user.password, listOf())
        )

        return JwtToken(token = _jwtTokenUtil.generateAccessToken(userIdentity.id.toString()))
    }

}