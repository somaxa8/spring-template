package com.somaxa8.template.service

import com.somaxa8.shared.exception.exceptions.ResourceNotFoundException
import com.somaxa8.shared.utils.BaseService
import com.somaxa8.template.domain.models.User
import com.somaxa8.template.mapper.UserMapper
import com.somaxa8.template.repository.UserRepository
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service

@Service
class UserService(
    private val _userRepository: UserRepository,
    private val _passwordEncoder: PasswordEncoder,
    private val _userMapper: UserMapper,
): BaseService<User, Long>(_userRepository) {

    override fun beforeCreate(entity: User): User {
        entity.id = null
        entity.password = _passwordEncoder.encode(entity.password)


        return entity
    }

    fun findByEmail(email: String): User {

        if (!existsByEmail(email)) {
            throw ResourceNotFoundException("User with email $email does not exists")
        }

        return _userRepository.findByEmail(email)
    }

    override fun beforeUpdate(id: Long, entity: User): User {
        var user = findById(id)

        user = _userMapper.fromUserToUser(entity, user)

        entity.password?.let {
            user.password = _passwordEncoder.encode(it)
        }

        return user
    }

    fun existsByEmail(email: String): Boolean {
        return _userRepository.existsByEmail(email)
    }

}
