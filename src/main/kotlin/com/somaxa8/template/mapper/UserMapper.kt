package com.somaxa8.template.mapper

import com.somaxa8.template.domain.models.User
import org.mapstruct.*

@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
interface UserMapper {

    @Mapping(target = "password", ignore = true)
    fun fromUserToUser(target: User, @MappingTarget source: User): User

}
