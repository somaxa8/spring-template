package com.somaxa8.template.controller

import com.somaxa8.shared.web.annotation.Paged
import com.somaxa8.shared.web.annotation.Pager
import com.somaxa8.template.domain.models.User
import com.somaxa8.template.security.SessionManager
import com.somaxa8.template.service.UserService
import jakarta.validation.Valid
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

@RestController
class UserController(
    private val _userService: UserService,
    private val _sessionManager: SessionManager
) {

    @GetMapping("/users/{id}")
    fun getById(@PathVariable id: Long): User {
        return _userService.findById(id)
    }

    @Paged
    @GetMapping("/users")
    fun findAll(
        @ModelAttribute user: User,
        @Pager pageRequest: PageRequest,
    ): Page<User> {
        return _userService.findAll(user, pageRequest)
    }

    @PostMapping("/users")
    fun create(@Valid @RequestBody user: User): User {
        return _userService.create(user)
    }

    @PreAuthorize("@_sessionManager.requestingUserMatchesWithSessionId(#id) or hasAnyAuthority('ADMIN', 'SUPER_ADMIN')")
    @PatchMapping("/users/{id}")
    fun update(
        @PathVariable id: Long,
        @Valid @RequestBody user: User,
    ): User {
        return _userService.update(id, user)
    }

    @PreAuthorize("@_sessionManager.requestingUserMatchesWithSessionId(#id) or hasAnyAuthority('ADMIN', 'SUPER_ADMIN')")
    @DeleteMapping("/users/{id}")
    fun delete(@PathVariable id: Long) {
        return _userService.deleteById(id)
    }

}
