package com.somaxa8.template.controller

import com.somaxa8.template.domain.models.JwtToken
import com.somaxa8.template.domain.models.UserCredentials
import com.somaxa8.template.service.AuthService
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController


@RestController
class AuthController(
    private val _authService: AuthService
) {

    @PostMapping("/auth/login")
    fun login(@Validated @RequestBody userCredentials: UserCredentials): JwtToken {
        return _authService.authenticate(userCredentials)
    }
}