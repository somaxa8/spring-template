package com.somaxa8.template.security

import com.somaxa8.template.domain.models.User
import com.somaxa8.template.security.models.CustomUserDetails
import com.somaxa8.template.service.UserService
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Component

@Component("userDetailsService")
class CustomUserDetailsService(
    private var _userService: UserService
) : UserDetailsService {

    override fun loadUserByUsername(username: String): UserDetails {
        val user = _userService.findByEmail(username)
        return _createUserDetails(user)
    }

    private fun _createUserDetails(user: User): CustomUserDetails {
        return CustomUserDetails(
            id = user.id.toString(),
            username = user.username,
            password = user.password,
        )
    }

}