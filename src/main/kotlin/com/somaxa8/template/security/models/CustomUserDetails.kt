package com.somaxa8.template.security.models

import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails

data class CustomUserDetails(

    private val id: String?,

    private val username: String?,

    private val password: String?,

    private val authorities: Collection<SimpleGrantedAuthority>? = mutableListOf(),

    private val accountExpiredYn: Boolean = false,

    private val accountLockedYn: Boolean = false,

    private val credentialsExpiredYn: Boolean = false,

    private val enabledYn: Boolean = true

): UserDetails {

    override fun getUsername() = username

    override fun getPassword() = password

    override fun getAuthorities() = authorities

    override fun isAccountNonExpired() = !accountExpiredYn

    override fun isAccountNonLocked() = !accountLockedYn

    override fun isCredentialsNonExpired() = !credentialsExpiredYn

    override fun isEnabled() = enabledYn

}