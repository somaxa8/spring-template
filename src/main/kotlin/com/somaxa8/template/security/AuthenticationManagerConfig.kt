package com.somaxa8.template.security

import com.somaxa8.template.domain.models.UserIdentity
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetails

@Configuration
class AuthenticationManagerConfig(
    private var _customUserDetailsService: CustomUserDetailsService,
) {

    @Bean
    fun authenticationManager(): AuthenticationManager {
        return object : AuthenticationManager {

            override fun authenticate(authentication: Authentication): Authentication {
                val userIdentity = authentication.principal as UserIdentity
                val userDetails = _customUserDetailsService.loadUserByUsername(userIdentity.email)

                val successfulAuthentication = _createSuccessfulAuthentication(authentication, userDetails)

                SecurityContextHolder.getContext().authentication = successfulAuthentication

                return _createSuccessfulAuthentication(authentication, userDetails)
            }

            private fun _createSuccessfulAuthentication(authentication: Authentication, user: UserDetails): Authentication {
                val token = UsernamePasswordAuthenticationToken(user, authentication.credentials, user.authorities)
                token.details = authentication.details
                return token
            }
        }
    }
}