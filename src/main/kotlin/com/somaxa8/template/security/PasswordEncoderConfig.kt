package com.somaxa8.template.security

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.crypto.bcrypt.BCrypt
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder

@Configuration
class PasswordEncoderConfig {

    @Bean
    fun passwordEncoder(): PasswordEncoder {
        return object : BCryptPasswordEncoder() {
            override fun encode(rawPassword: CharSequence): String {
                return BCrypt.hashpw(rawPassword.toString(), BCrypt.gensalt(12))
            }

            override fun matches(rawPassword: CharSequence, encodedPassword: String): Boolean {
                return BCrypt.checkpw(rawPassword.toString(), encodedPassword)
            }
        }
    }

}
