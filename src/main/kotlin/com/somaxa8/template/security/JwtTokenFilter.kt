package com.somaxa8.template.security

import com.somaxa8.template.domain.models.Authority
import com.somaxa8.template.domain.models.UserIdentity
import com.somaxa8.template.security.util.JwtTokenUtil
import com.somaxa8.template.service.UserService
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import jakarta.servlet.FilterChain
import jakarta.servlet.ServletException
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import java.io.IOException


@Component
class JwtTokenFilter(
    private val _jwtTokenUtil: JwtTokenUtil,
    private val _userService: UserService,
): OncePerRequestFilter() {

    @Throws(ServletException::class, IOException::class)
    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain) {

        val token = _resolveToken(request)

        if (token == null) {
            chain.doFilter(request, response)
            return
        }

        _jwtTokenUtil.validate(token)

        val subject = _jwtTokenUtil.resolveSubject(token)

        val user = _userService.findById(subject.toLong())
        val auth = UsernamePasswordAuthenticationToken(
            UserIdentity(user.email!!, user.id!!),
            user.password,
            Authority.getSimpleGrantedAuthoritiesFrom(user.authorities)
        )

        auth.details = WebAuthenticationDetailsSource().buildDetails(request)
        SecurityContextHolder.getContext().authentication = auth

        chain.doFilter(request, response)
    }

    private fun _resolveToken(request: HttpServletRequest): String? {
        val authHeader = request.getHeader("Authorization")

        if (authHeader.isNullOrEmpty()) {
            return null
        }

        return authHeader
            .split(" ".toRegex())
            .dropLastWhile { it.isEmpty() }
            .toTypedArray()[1]
            .trim { it <= ' ' }
    }
}