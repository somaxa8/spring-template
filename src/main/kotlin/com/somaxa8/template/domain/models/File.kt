package com.somaxa8.template.domain.models

import com.somaxa8.shared.utils.Identifiable
import com.somaxa8.shared.validation.groups.Create
import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.EnumType
import jakarta.persistence.Enumerated
import jakarta.persistence.GeneratedValue
import jakarta.persistence.Id
import jakarta.persistence.Transient
import jakarta.validation.constraints.NotNull
import org.springframework.web.multipart.MultipartFile
import java.io.Serializable

@Entity
class File(
    @Id
    @GeneratedValue
    override var id: Long? = null,

    @Column(nullable = false)
    var name: String? = null,

    @Column(nullable = false)
    var baseName: String? = null,

    @Column(nullable = false)
    var extension: String? = null,

    @field:NotNull
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    var type: Type? = null,

    @Column(nullable = false)
    var path: String? = null,

    @field:NotNull(groups = [Create::class])
    @Transient
    var file: MultipartFile? = null,

    ): Identifiable<Long>, Serializable {

    enum class Type {
        IMAGE, SOUND
    }

    @get:Transient
    val url: String
        get() = "$path/$name"

}