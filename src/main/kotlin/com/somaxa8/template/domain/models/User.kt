package com.somaxa8.template.domain.models

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import com.somaxa8.shared.utils.Identifiable
import jakarta.persistence.*
import jakarta.validation.constraints.Email
import jakarta.validation.constraints.NotNull

@Entity
@Table(name = "_user")
class User(
    @Id
    @GeneratedValue
    override var id: Long? = null,

    @field:NotNull
    @Column(nullable = false)
    var username: String? = null,

    @field:NotNull
    @field:Email
    @Column(unique = true, nullable = false)
    var email: String? = null,

    @field:NotNull
    @field:JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(nullable = false)
    var password: String? = null,

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
        name = "rel_user_authority",
        inverseJoinColumns = [JoinColumn(name = "authority_id", referencedColumnName = "id")],
        joinColumns = [JoinColumn(name = "user_id", referencedColumnName = "id")]
    )
    var authorities: MutableSet<Authority> = mutableSetOf(),
): Identifiable<Long>
