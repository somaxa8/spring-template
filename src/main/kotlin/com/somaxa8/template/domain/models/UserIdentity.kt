package com.somaxa8.template.domain.models

class UserIdentity(
    var email: String,
    var id: Long,
)