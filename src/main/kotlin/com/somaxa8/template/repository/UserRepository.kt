package com.somaxa8.template.repository

import com.somaxa8.template.domain.models.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
@Repository
interface UserRepository: JpaRepository<User, Long>{

    fun findByEmail(email: String): User
    fun existsByEmail(email: String): Boolean

}
