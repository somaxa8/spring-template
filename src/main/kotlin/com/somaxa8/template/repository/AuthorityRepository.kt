package com.somaxa8.template.repository

import com.somaxa8.template.domain.models.Authority
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface AuthorityRepository : JpaRepository<Authority, Authority.Role> {
}
