<!---------------------------------------------------------------------------->
<!-- Briefly describe the error/bug/problem -->

{DESCRIPTION}

<!---------------------------------------------------------------------------->
# Module/Component/Service
<!--
Which element is being affected?
-->

1. {ELEMENT 1}
1. {ELEMENT 2}

<!---------------------------------------------------------------------------->
# Steps to reproduce
<!--
Be as descriptive as possible and try to reproduce the error more than once.
-->

1. {Login}
2. {Go to XXX}

<!---------------------------------------------------------------------------->
# Current result (Screenshot, error log)
<!--
Screenshots template:

| Title  |    Description    |               Screen              |
|--------|-------------------|-----------------------------------|
|My Title|A brief description|![Image](https://image.url/file.ext)

-->

<!-- For large errors use this -->
<details>
	<summary>Error log:</summary>
	<pre>

        {ERROR_BACKTRACE}

	</pre>
</details>

<!---------------------------------------------------------------------------->
# Expected result
<!--
What is expected to happen? How should be the happy path?
-->


{EXPECTED_RESULT}

<!---------------------------------------------------------------------------->
# Additional info
<!--........................................................................-->
# Possible cause of the error
<!--
Do you see a pattern or a reason for the error? Describe it here.
-->

{POSSIBLE_CAUSE}

<!---------------------------------------------------------------------------->
<!-- Please don't change this lines -->
/label ~"Type::Bug"

