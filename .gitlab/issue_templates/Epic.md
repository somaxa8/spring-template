<!---------------------------------------------------------------------------->
<!-- Describe the new feature -->

{DESCRIPTION}

<!---------------------------------------------------------------------------->
# Tasks
<!--
Which tasks needs to be done in order to finish this requirement?

The first task that should be performed is the refinement in order to understand
the task scope.

Tasks should be defined as:

* [ ] #{N} {Description}

where:

* {N} Is the number/id of created issue.
* {Description} is the actual description/title defined in the issue.
-->

* [ ] ~"Type::Refinement" Task: #nnn
* [ ] #nnn Task 1
* [ ] #nnn Task 2
* [ ] #nnn Task 3

<!---------------------------------------------------------------------------->
# Must-have checklist
<!--
Which criteria should the implementation have to be considered done?
-->

* [ ] First criteria.
* [ ] Second criteria.

<!---------------------------------------------------------------------------->
# Additional info
<!--........................................................................-->
## Design Proposal

{C4_MODEL, DIAGRAMS}

<!--........................................................................-->
## Design Notes

{DESIGN_NOTES}

<!---------------------------------------------------------------------------->
<!-- Please don't change this lines -->
/label ~"Type::Epic"

